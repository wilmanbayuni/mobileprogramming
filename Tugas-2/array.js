// Range
function range(startNum, finishNum)
{
if (startNum == null || finishNum == null)
{
return -1;
}
else if (startNum < finishNum)
{
var hasil = []
for (i=startNum; i<=finishNum; i++)
{
hasil.push (i)
}
return hasil;
}
else if (startNum > finishNum)
{
var hasil = []
for (i=startNum; i>finishNum; i--)
{
hasil.push (i)
}
return hasil;
}
}
console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range())

//rangeWithStep
function rangeWithStep(startNum, finishNum, step)
{
if (startNum == null || finishNum == null)
{
return -1;
}
else if (startNum < finishNum)
{
var hasil = []
for (i=startNum; i<=finishNum; i+=step)
{
hasil.push (i)
}
return hasil;
}
else if (startNum > finishNum)
{
var hasil = []
for (i=startNum; i>finishNum; i-=step)
{
hasil.push (i)
}
return hasil;
}
}
console.log(rangeWithStep(1, 10, 2)) 
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

//sum of range
function sum(startNum, finishNum, deret)
{
if (deret == null)
{
deret = 1
}
if (startNum == null && finishNum == null)
{
return 0;
}
else if (finishNum == null)
{
return 1;
}
else if (startNum < finishNum)
{
var hasil = 0
for (i=startNum; i<=finishNum; i+=deret)
{
hasil = hasil + i
}
return hasil;
}
else if (startNum > finishNum)
{
var hasil = 0
for (i=startNum; i>=finishNum; i-=deret)
{
hasil = hasil + i
}
return hasil;
}
}
console.log(sum(1,10)) 
console.log(sum(5, 50, 2)) 
console.log(sum(15,10)) 
console.log(sum(20, 10, 2)) 
console.log(sum(1)) 
console.log(sum())

console.log("\n")
// array multidimensi
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989",
    "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970",
    "Berkebun"]
    ]
    console.log("Nomor id: "+input[0][0]+"\nNama: "+input[0][1]+"\nAlamat: "+input[0][2]+"\nTanggal Lahir: "+input[0][3]+"\nHobby: "+input[0][4]);
    console.log("\nNomor id: "+input[1][0]+"\nNama: "+input[1][1]+"\nAlamat: "+input[1][2]+"\nTanggal Lahir: "+input[1][3]+"\nHobby: "+input[1][4]);
    console.log("\nNomor id: "+input[2][0]+"\nNama: "+input[2][1]+"\nAlamat: "+input[2][2]+"\nTanggal Lahir: "+input[2][3]+"\nHobby: "+input[2][4]);
    console.log("\nNomor id: "+input[3][0]+"\nNama: "+input[3][1]+"\nAlamat: "+input[3][2]+"\nTanggal Lahir: "+input[3][3]+"\nHobby: "+input[3][4]);
    
    console.log("\n")
// Balik Kata
function balikKata(kata)
{
var katabaru = " "
for (i=(kata.length-1); i>=0; i--)
{
katabaru = katabaru+kata[i];
}
return katabaru;
}
console.log(balikKata(" Kasur Rusak ")) 
console.log(balikKata(" Informatika ")) 
console.log(balikKata(" Haji Ijah ")) 
console.log(balikKata(" racecar ")) 
console.log(balikKata(" I am Humanikers " ))

//metode array
var input = ["0001", "Roman Alamsyah Elsharawy", "Bandar Lampung", "21/5/1989", "Membaca"];
var data1 = input[0]
var data2 = input[1]
var data3 = input[2]
var data4 = input[3]
var data5 = ('SMA Internasional Metro')
var alamat = data3.split(' ')
alamat.unshift('Provinsi')
var newalamat = alamat.join(" ")
var output = (data1+","+data2+","+newalamat+","+data4+",Pria,"+data5)
var newoutput = output.split(",")
console.log(newoutput)
var tanggal = data4.split('/')
var newtanggal = tanggal.join("-")
var bulan = tanggal[1] = 5
switch(bulan){
	case 1: {console.log('Januari'); break;}
	case 2: {console.log('Februari'); break;}
	case 3: {console.log('Maret'); break;}
	case 4: {console.log('April'); break;}
	case 5: {console.log('Mei'); break;}
	case 6: {console.log('Juni'); break;}
	case 7: {console.log('Juli'); break;}
	case 8: {console.log('Agustus'); break;}
	case 9: {console.log('September'); break;}
	case 10: {console.log('Oktober'); break;}
	case 11: {console.log('November'); break;}
	case 12: {console.log('Desember'); break;}
default: {console.log('Tanggal tidak tersedia'); break;}}
var numbers = tanggal
numbers.sort()
var nama = data2.split(' ')
nama.pop()
var newnama = nama.join(" ")
console.log(numbers)
console.log(newtanggal)
console.log(newnama)
