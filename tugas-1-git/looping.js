console.log("Looping Pertama");
var i = 1;
var n = 20;

while (i <= n) {
    if (i % 2 == 0) {
        console.log(i + " " + 'I love coding');
    }
    i++;
}
console.log("\nLooping Kedua");
var a = 20;
while (a >= 2) {
    console.log(a + " " + 'I will become a mobile developher');
    a -= 2;
}

console.log("\n");
console.log("Looping For");
var a = 20;
for (b = 1; b <= a; b++) {
    if (b % 2 == 0) {
        console.log(b + " " + "Informatika");
    }
    else if (b % 3 == 0) {
        console.log(b + " " + 'I Love Coding');
    }
    else {
        console.log(b + " " + "Teknik");
    }
}

console.log("\n");

console.log('Persegi Panjang\n');
var a = 4;
var persegi = 8;
for (b = 1; b <= a; b++) {
    var kotak = "#".repeat(persegi);
    console.log(kotak);
}

console.log("\n");

console.log('Tangga\n');
var a = 1;
var b = 7;

for (a = 1; a <= b; a++) {
    var Tangga = "#".repeat(a);
    console.log(Tangga);
}

console.log("\n");

console.log('Papan Catur\n');
var b = 8;
var c = 4;
var hitam = "#";
var putih = " ";
for (a = 1; a <= b; a++) {
    var papan = (hitam + putih).repeat(c);
    console.log(papan);
}


